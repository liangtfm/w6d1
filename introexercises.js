Array.prototype.include = function (value) {
  for (var i=0;i<this.length;i++){
    if (this[i] === value) {
      return true;
    }
  }
  return false;
}

function uniq(array) {
  var newArray = [];

  for (var i=0; i < array.length; i++) {
    if (!newArray.include(array[i])) {
      newArray.push(array[i]);
    }
  }

  return newArray;
}

console.log(uniq([1,2,3,1,2]));


Array.prototype.twoSum = function () {
  var twoSums = [];

  for (var i=0; i < this.length; i++) {
    for (var j=i+1; j < this.length;j++) {
      if (this[i] + this[j] === 0) {
        twoSums.push([i,j]);
      }
    }
  }

  return twoSums;
}

console.log([-1,0,2,-2,1].twoSum());


Array.prototype.transpose = function () {
  var transArray = [];

  for (var i=0; i < this.length; i++) {
    var row = [];
    for (var j=0; j < this.length; j++) {
      row.push(this[j][i]);
    }

    transArray.push(row);
  }

  return transArray;
}

console.log([[0, 1, 2],[3, 4, 5],[6, 7, 8]].transpose());