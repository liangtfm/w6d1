var _ = require('underscore');

Array.prototype.bubbleSort = function() {

  for (var i = 0; i < this.length; i++) {

    for(var j = i + 1; j < this.length; j++){
      if (this[i] > this[j]){
        var temp = this[j];
        this[j] = this[i];
        this[i] = temp;
      }

    }

  }

  return this;
}

// console.log([1, 2, 3, 4, 5].bubbleSort());

Array.prototype.include = function (value) {
  for (var i=0;i<this.length;i++){
    if (this[i] === value) {
      return true;
    }
  }
  return false;
}

var substrings = function(string) {
  var newArray = [];

  function range (x,y) {
    var rangeArray = [];

    for(var i=x; i <= y; i++) {
      rangeArray.push(string[i]);
    }

    return rangeArray;
  }

  for (var i = 0; i < string.length; i++) {

    for (var j = i; j < string.length; j++) {
      var substring = range(i,j);

      if (!newArray.include(substring)) {
        newArray.push(substring);
      }

    }

  }

  return newArray;
}

// console.log(substrings("cat"));



