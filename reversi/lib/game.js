var Piece = require("./piece.js").Piece;
var Board = require("./board.js").Board;

function Game() {
	this.board = new Board();
	// Other attributes?
};

// You will certainly need some more helper methods...

Game.prototype.won = function() {
  return this.board.full();
};

Game.prototype.winner = function() {
  if (!this.won()) {
    return null;
  } else {
    var pieces = this.board.countPieces();
    var whitePieces = pieces[0];
    var blackPieces = pieces[1];

    return (whitePieces < blackPieces ? "black" : "white");
  }
}

Game.prototype.placePiece = function(pos, color) {
  try {
    this.board.placePiece(pos, color);
  } catch(error){
    console.log("Invalid Move")
  }
};

Game.prototype.runLoop = function() {

};

exports.Game = Game;