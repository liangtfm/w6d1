var Piece = require("./piece.js").Piece;

var directions = [
  [0, 1],  [0, -1],
  [1, 1],  [1, 0],
  [1, -1], [-1, -1],
  [-1, 0], [-1, 1]
  ];

function Board(){
  this.count = 4;

  this.grid = [];
  for (var i = 0; i < 8; i++) {
    this.grid[i] = []
    for (var j = 0; j < 8; j++) {
      this.grid[i][j] = null;
    }
  }

  this.grid[3][3] = new Piece("white");
  this.grid[4][4] = new Piece("white");
  this.grid[3][4] = new Piece("black");
  this.grid[4][3] = new Piece("black");


};

Board.prototype.full = function() {
  return (this.count === 64);
};

Board.prototype.placePiece = function(pos, color) {
  var x = pos[0];
  var y = pos[1];
  var toFlip = this.flipPieces(pos, color);

  if (!this.grid[x][y] && this.hasNeighbors(pos) && toFlip.length > 0) {
    this.grid[x][y] = new Piece(color);
    this.count += 1;

    for (var i = 0; i < toFlip.length; i++) {
      toFlip[i].flip();
    }

  } else {
    throw("Invalid Move");
  }

}

Board.prototype.flipPieces = function(pos, color) {
  var x = pos[0];
  var y = pos[1];
  var toFlip = [];

  for(var i = 0; i < directions.length; i++) {
    var piece = this.flipLine(pos, directions[i], color);
    if (piece)
    toFlip = toFlip.concat(piece);
  }

  return toFlip;
}

Board.prototype.flipLine = function(pos, dir, color) {
  var xdir = dir[0];
  var ydir = dir[1];
  var curx = pos[0] + xdir;
  var cury = pos[1] + ydir;
  var toFlip = [];

  while ( !this.outOfBounds(curx, cury) && this.grid[curx][cury] !== null &&
          this.grid[curx][cury].color !== color) {

    toFlip.push(this.grid[curx][cury]);
    curx += xdir;
    cury += ydir;
  }

  if (this.grid[curx][cury] && (this.grid[curx][cury].color === color)) {
  //   for (var i = 0; i < toFlip.length; i++) {
  //     toFlip[i].flip();
  //   }
    return toFlip;
  }

}

Board.prototype.hasNeighbors = function(pos) {
  var x = pos[0];
  var y = pos[1];
  neighborCount = 0;

  for (var i = 0; i < 8; i++) {
    if (!this.outOfBounds(pos) && this.grid[x][y]) {
      neighborCount++;
    }
  }

  return (neighborCount == 0);
}

Board.prototype.outOfBounds = function(pos) {
  var x = pos[0];
  var y = pos[1];

  if (x > 7 || x < 0 || y > 7 || y < 0 ) {
    return true;
  }

  return false;
}

Board.prototype.countPieces = function() {
  var whitePieces = 0;
  var blackPieces = 0;

  for (var i=0; i<8; i++) {
    for (var j=0; j<8; j++) {
      if (this.grid[i][j] && this.grid[i][j].color === "black") {
        blackPieces++;
      } else if (this.grid[i][j] && this.grid[i][j].color === "white") {
        whitePieces++;
      }
    }
  }

  return [whitePieces, blackPieces];
}



// Other helper methods may be helpful!

exports.Board = Board;

// var b = new Board();
//
// console.log(b.grid);
//
// b.placePiece([5, 4], "black");
// console.log(b.grid);
//
// b.placePiece([5, 5], "white");
// console.log(b.grid);
//
// b.placePiece([4, 5], "black");
// console.log(b.grid);
//
// b.placePiece([5, 3], "white");
// console.log(b.grid);
