function Piece(color){
	this.color = color;
};

Piece.prototype.flip = function() {
  this.color = (this.color === "white" ? "black" : "white");
};

Piece.prototype.inspect = function() {
  return (this.color === "white" ? " W  " : " B  ");
}

exports.Piece = Piece;