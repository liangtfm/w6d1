Array.prototype.multiples = function () {
  newArray = [];

  for (var i=0; i < this.length; i++) {
    newArray.push(this[i]*2);
  }

  return newArray;
}

// console.log([1,2,3,4,5].multiples());

Array.prototype.myEach = function(func) {
  for (var i = 0; i < this.length; i++) {
    func(this[i]);
  }

  return this;
}

var array = [1, 2, 3, 4, 5];
// array.myEach(print);

function print(value) {
  console.log(value);
}


Array.prototype.myMap = function(func) {
  var newArray = [];

  function callFunc (value) {
    newArray.push(func(value));
  }

  this.myEach(callFunc);
  return newArray;
}

function timesTwo (value) {
  return value * 2;
}

// console.log(array.myMap(timesTwo));


Array.prototype.myInject = function(func) {
  var sum = 1;
  var newArray = [];

  for(var i = 1; i < this.length; i++) {
    newArray.push(this[i]);
  }

  function callFunc (x) {
    sum = func(sum, x);
  }

  newArray.myEach(callFunc);

  return sum;
}

function add (x, y){
  return x + y;
}

function multiply (x, y) {
  return x * y;
}

console.log([1, 2, 3, 4, 5].myInject(add));