Array.prototype.include = function (value) {
  for (var i=0;i<this.length;i++){
    if (this[i] === value) {
      return true;
    }
  }
  return false;
}

function Student (firstName, lastName) {
  this.firstName = firstName,
  this.lastName = lastName,
  this.courses = []
}

Student.prototype.name = function() {
  return firstName + " " + lastName;
}

Student.prototype.enroll = function(course) {
  if (this.hasConflict(course)) {
    console.log("Conflict!");
  } else {
    this.courses.push(course);
    course.addStudent(this);
  }
}

Student.prototype.hasConflict = function(course) {
  for (var i = 0; i < this.courses.length; i++) {
    if (this.courses[i].conflictsWith(course)) {
      return true;
    }
  }

  return false;
}

Student.prototype.courseLoad = function() {
  var hash = {};
  for (var i = 0; i < this.courses.length; i++) {
    if (hash[this.courses[i].dept]){
      hash[this.courses[i].dept] += this.courses[i].credits;
    } else {
      hash[this.courses[i].dept] = this.courses[i].credits;
    }
  }

  return hash;
}

Student.prototype.toString = function() {
  return this.name();
}


function Course (name, dept, credits, days, time) {
  this.name = name,
  this.dept = dept,
  this.credits = credits,
  this.students = [],
  this.days = days,
  this.time = time
}

Course.prototype.addStudent = function(student) {
  this.students.push(student);
}

Course.prototype.conflictsWith = function(course) {
  if (this.time === course.time) {
    for (var i=0; i < this.days.length; i++) {
      if (course.days.include(this.days[i])) {
        return true;
      }
    }
  }

  return false;
}

Course.prototype.toString = function() {
  return this.name;
}


var s1 = new Student("bob", "bobby");

var geo = new Course("geology", "GEOL", 3, ["mon", "wed", "fri"], 1);
var phys = new Course("physics 200", "PHYS", 4, ["tues", "thurs"], 2);
var cs = new Course("compsci 100", "CSCI", 4, ["mon", "fri"], 1);
var cs2 = new Course("compsci 200", "CSCI", 3, ["thurs", "fri"], 1);

s1.enroll(geo);
s1.enroll(phys);
s1.enroll(cs);
s1.enroll(cs2);

console.log(s1.courseLoad());
console.log(s1.courses);
console.log(cs2.students);
console.log(cs2.toString());

console.log(cs.conflictsWith(cs2));
console.log(geo.conflictsWith(phys));