var rangeIter = function (x, y) {
  var rangeArray = [];

  for(var i=x; i <= y; i++) {
    rangeArray.push(i);
  }

  return rangeArray;
}

var rangeRecursive = function(start, end) {
  var rangeArray = [];

  if (start === end) {
    return start;
  } else {
    var value = rangeRecursive(start + 1, end);
    rangeArray.push(start);
    rangeArray = rangeArray.concat(value);
  }

  return rangeArray;
}

// console.log(rangeRecursive(1, 8));

var exp1 = function (num, exp) {
  if (exp === 0) {
    return 1;
  } else {
    var value = exp1(num, exp - 1);
    return num * value;
  }
}

// console.log(exp1(2, 4));

var exp2 = function(num, exp) {
  if (exp === 0) {
    return 1;
  } else if (exp % 2 === 0) {
    var value = Math.pow(exp2(num, exp / 2), 2);
  } else {
    var value = num * Math.pow(exp2(num, (exp - 1) / 2), 2);
  }

  return value;
}

// console.log(exp2(3, 3));


var fibs = function(num) {
  if (num === 0) {
    return [];
  } else if (num === 1) {
    return [0];
  } else if (num === 2) {
    return [0,1];
  } else {
    var newArray = fibs(num-1);
    newArray.push(newArray[newArray.length-1] + newArray[newArray.length-2]);
  }

  return newArray;
}

// console.log(fibs(10));

Array.prototype.range = function (x,y) {
  var rangeArray = [];

  for(var i=x; i <= y; i++) {
    rangeArray.push(this[i]);
  }

  return rangeArray;
}

Array.prototype.include = function (value) {
  for (var i=0;i<this.length;i++){
    if (this[i] === value) {
      return true;
    }
  }
  return false;
}

Array.prototype.empty = function() {
  return this.length == 0;
}

var bSearch = function (array, target) {
  if (array.length === 1) {

    if (array[0] === target) {
      return 0;
    } else {
      return null;
    }
  } else {
    var middle = Math.floor(array.length - 1 / 2);
    var low, high;

    if (array[middle] === target) {
      return middle;

    } else if (target > array[middle]) {
      low = middle + 1;
      high = array.length - 1;

      return middle + bSearch(array.range(low,high), target);
    } else {
      low = 0;
      high = middle - 1;

      return bSearch(array.range(low,high), target);
    }
  }
}

// console.log(bSearch([1,2,3,4,5,6,7,8,9], 5));


var makeChange = function(amount, coins) {

  if (coins.include(amount)) {
    return [amount];
  } else {
    var change = []

    for (var i = 0; i < coins.length; i++) {
      if (amount > coins[i]) {
        change.push(coins[i]);
        amount -= coins[i];

        return change.concat(makeChange(amount, coins));
      }
    }
  }

}

var makeSlowChange = function(amount, coins) {
  var change1 = makeChange(amount, coins);
  var change2 = makeChange(amount, coins.range(1, coins.length - 1));

  return (change1.length < change2.length ? change1 : change2)
}

// var coins = [10, 7, 1];
// console.log(makeSlowChange(15, coins));

var merge = function(left, right) {
  var mergedArray = [];
  while (!left.empty() && !right.empty()) {
    if (left[0] < right[0]) {
      mergedArray.push(left.shift());
    } else {
      mergedArray.push(right.shift());
    }
  }

  if (!left.empty()) {
    mergedArray = mergedArray.concat(left);
  } else if (!right.empty()) {
    mergedArray = mergedArray.concat(right);
  }

  return mergedArray;
}

// console.log(merge([4,5,6], [1,2,3]));

var mergeSort = function(array) {

  if (array.length === 1) {
    return array;
  } else {
    var left = array.range(0, Math.floor((array.length-1)/2));
    var right = array.range(Math.floor((array.length+1)/2), array.length-1);

    left = mergeSort(left);
    right = mergeSort(right);

    return merge(left, right);
  }

}

// array = [1,4,6,8,2,5];
// console.log(mergeSort(array));


var subsets = function(array) {
  if (array.empty()) {
    return [array];
  } else if (array.length === 1) {
    return [ [], array ];
  } else {
    var newArray = subsets(array.range(0, array.length - 2));
    var len = newArray.length
    console.log("array " + array);

    for (var i = 0; i < len; i++) {
      newArray.push( newArray[i].concat(array[array.length - 1]) );
    }

    return newArray;
  }
}

// console.log([1, 2, 3].range(0, 1));
// console.log(subsets([1, 2, 3, 4, 5, 6, 7, 8, 9]));