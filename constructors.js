function Cat (name, owner) {
  this.name = name,
  this.owner = owner
}

Cat.prototype.cuteStatement = function() {
  return "everyone loves " + this.name;
}

Cat.prototype.meow = function() {
  return "meow";
}

var whiskers = new Cat("whiskers", "bob");
var hank = new Cat("hank", "jeff");
hank.meow = function() {
  return "meow MEOW! I am hank!";
}

console.log(whiskers.cuteStatement());
console.log(hank.cuteStatement());

console.log(whiskers.meow());
console.log(hank.meow());